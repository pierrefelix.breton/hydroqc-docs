+++
title = "Dons"
linkTitle = "Dons"
description = "Supportez le développement du projet Hydroqc"

[menu.main]
weight = 30
+++
{{< blocks/lead color="dark" >}}
<h1>Dons</h1>
<p>
L'équipe du projet Hydroqc donne énormément de son temps à développer, documenter, supporter et améliorer les différents composants du projet et met l'effort pour livrer une solution open-source, accessible, de qualité professionnelle, 100% gratuite et qui respecte votre vie privée.
</p>
<p>
Si vous désirez soutenir les efforts de l'équipe et nous signifier votre appui vous pouvez le faire en fesant un don ponctuel ou récurrent.
</p>
<br>
<p>

<a href="https://www.paypal.com/donate/?hosted_button_id=GRFRXT6L8GCR8" title="Paypal" style="color: #ffffff"><i class="fab fa-cc-paypal fa-3x" ></i></a>

<a href="bitcoin:bc1qdn3rntxu3300wzwkya43u7x09r8h9mjmsrpkr6?label=Projet%20Hydroqc&message=Don%20pour%20Hydroqc"<span style="color: #ffffff"> <i class="fab fa-bitcoin fa-3x"></i></span> </a>

<a href="ethereum:0xBF5d8E49600c110d2D570E903cc63404254F29F7?label=Projet%20Hydroqc&message=Don%20pour%20Hydroqc"<span style="color: #ffffff"> <i class="fab fa-ethereum fa-3x"></i></span></a>

<a href="monero:86NdjqxSKM7jjwKvoX3CBkYd8dX7nxzBkZL7DeYsEC7F9MGJmBQC7zMZ5wjCk9beqThDoGRb7x3ekeMPdbUh3t8Y2pq8qhB?recipient_name=Projet%20Hydroqc&tx_description=Don%20pour%20Hydroqc"<span style="color: #ffffff"> <i class="fab fa-monero fa-3x"></i></span>
</p>

{{< /blocks/lead >}}


{{< blocks/section color="primary" type="row" >}}

{{% blocks/feature icon="fab fa-gitlab" title="Bienvenue aux contributeurs!" url="https://gitlab.com/hydroqc" %}}
Nous acceptons les [Merge Request](https://gitlab.com/groups/hydroqc/-/merge_requests) sur **Gitlab**. Les nouveaux contributeurs sont les bienvenues!
{{% /blocks/feature %}}

{{% blocks/feature icon="fab fa-discord" title="Venez discuter sur Discord!" url="https://discord.gg/BTPDntfaXH" %}}
Pour le soutien, clavardé et les annonces des dernières fonctionnalités.
{{% /blocks/feature %}}

{{% blocks/feature icon="fa-solid fa-hand-holding-heart" title="Dons" url="/fr/dons" %}}
Supportez l'équipe du projet Hydroqc
{{% /blocks/feature %}}

{{< /blocks/section >}}


