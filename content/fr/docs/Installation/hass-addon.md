---
title: Home-Assistant Addon
linkTitle: Home-Assistant Addon
weight: 14
description: |
  Installation avec le addon Home-Assistant
lastmod: 2022-09-21T19:50:59.393Z
---

**Option 1**

Utilisez le bouton suivant:
[![Open your Home Assistant instance and show the add add-on repository dialog with a specific repository URL pre-filled.](https://my.home-assistant.io/badges/supervisor_add_addon_repository.svg)](https://my.home-assistant.io/redirect/supervisor_add_addon_repository/?repository_url=https%3A%2F%2Fgitlab.com%2Fhydroqc%2Fhydroqc-hass-addons)

**Option 2**

Dans la section "Supervisor" -> Add-on Store -> cliquez sur les "..." verticaux en haut à droite et ajoutez : https://gitlab.com/hydroqc/hydroqc-hass-addons.git

Une fois terminé, accédez à l'addon hydroqc et cliquez sur Installer.
