---
title: Suivit des coûts
linkTitle: Suivit des coûts
weight: 28
description: |
  Configurations spécifiques à Home-Assistant
lastmod: 2023-11-08T00:32:42.429Z
date: 2023-11-08T00:32:44.060Z
---

## Suivi des coûts

Il n'y a aucun moyen précis de suivre le prix des tarifs actuellement, car tous les capteurs disponibles suivent les données de la veille.

Si vous ne vous souciez pas de l'exactitude, vous pouvez également ajouter le capteur "Current billing period total to date" sous l'option "Utiliser une entité de suivi des coûts totaux". Cela affichera les données de taux, mais elle le calculera le prix d'hier avec la consommation du jour en cours.

