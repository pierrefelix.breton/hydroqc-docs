---
title: Home-Assistant
linkTitle: Home-Assistant
weight: 20
description: |
  Configurations spécifiques à Home-Assistant
aliases:
  - /fr/docs/configuration/home-assistant-specific/
lastmod: 2022-09-20T13:08:04.762Z
date: 2023-11-08T00:32:58.689Z
---
