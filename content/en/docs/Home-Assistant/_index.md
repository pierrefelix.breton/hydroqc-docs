---
title: Home-Assistant
linkTitle: Home-Assistant
weight: 20
description: |
  Home-Assistant specific configurations
aliases:
  - /en/docs/configuration/home-assistant-specific/
lastmod: 2023-11-08T00:33:22.196Z
date: 2023-11-08T00:33:20.196Z
---
