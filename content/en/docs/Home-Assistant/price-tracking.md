---
title: Price tracking
linkTitle: Price tracking
weight: 28
description: |
  Configurations for price tracking
lastmod: 2023-11-08T00:33:47.016Z
date: 2023-11-08T00:33:45.072Z
---

### Price tracking

There is no accurate way to track the rate price currently since all the sensors available track data from the day before.

If you don't care about accuracy, you can also add the "Current billing period total to date" sensor under the option "Use an entity tracking the total costs". This will display rate data but it will calculate it with the price from yesterday with the consumption of the current day.



