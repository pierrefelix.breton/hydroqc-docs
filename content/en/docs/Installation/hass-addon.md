---
title: Home-Assistant Addon
linkTitle: Home-Assistant Addon
weight: 14
description: |
  Install with the Home-Assistant Addon
lastmod: 2022-09-21T19:50:59.393Z
---

**Option 1**

Click this button:
[![Open your Home Assistant instance and show the add add-on repository dialog with a specific repository URL pre-filled.](https://my.home-assistant.io/badges/supervisor_add_addon_repository.svg)](https://my.home-assistant.io/redirect/supervisor_add_addon_repository/?repository_url=https%3A%2F%2Fgitlab.com%2Fhydroqc%2Fhydroqc-hass-addons)

**Option 2**

Go in the supervisor page -> Add-on Store -> click on the vertical "..." on the top right of the page, add this repository: https://gitlab.com/hydroqc/hydroqc-hass-addons.git

Once completed, go into the hydroqc addon and click install.
