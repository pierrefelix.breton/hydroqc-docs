+++
title = "Donations"
linkTitle = "Donations"
description = "Support the development of the Hydroqc project"

[menu.main]
weight = 30
+++
{{< blocks/lead color="dark" >}}
<h1>Dons</h1>
<p>
The Hydroqc project team gives a lot of its time to develop, document, support and improve the different components of the project and puts the effort to deliver an open-source, accessible, professional quality, 100% free solution that respects yourprivate life.
</p>
<p>
If you want to support the team's efforts and show your appreciation you can do so by making a one-time or recurring donation.
</p>
<p>

<a href="https://www.paypal.com/donate/?hosted_button_id=GRFRXT6L8GCR8" title="Paypal" style="color: #ffffff"><i class="fab fa-cc-paypal fa-3x" ></i></a>

<a href="bitcoin:bc1qdn3rntxu3300wzwkya43u7x09r8h9mjmsrpkr6?label=Projet%20Hydroqc&message=Don%20pour%20Hydroqc"<span style="color: #ffffff"> <i class="fab fa-bitcoin fa-3x"></i></span> </a>

<a href="ethereum:0xBF5d8E49600c110d2D570E903cc63404254F29F7?label=Projet%20Hydroqc&message=Don%20pour%20Hydroqc"<span style="color: #ffffff"> <i class="fab fa-ethereum fa-3x"></i></span></a>

<a href="monero:86NdjqxSKM7jjwKvoX3CBkYd8dX7nxzBkZL7DeYsEC7F9MGJmBQC7zMZ5wjCk9beqThDoGRb7x3ekeMPdbUh3t8Y2pq8qhB?recipient_name=Projet%20Hydroqc&tx_description=Don%20pour%20Hydroqc"<span style="color: #ffffff"> <i class="fab fa-monero fa-3x"></i></span>
</p>

</p>

{{< /blocks/lead >}}



{{< blocks/section color="primary" type="row" >}}


{{% blocks/feature icon="fab fa-gitlab" title="Contributions welcome!" url="https://gitlab.com/hydroqc" %}}
We do a [Merge Request](https://gitlab.com/groups/hydroqc/-/merge_requests) contributions workflow on **Gitlab**. New users are always welcome!
{{% /blocks/feature %}}

{{% blocks/feature icon="fab fa-discord" title="Join us on Discord!" url="https://discord.gg/BTPDntfaXH" %}}
For support, discussion and announcement of latest features.
{{% /blocks/feature %}}

{{% blocks/feature icon="fa-solid fa-hand-holding-heart" title="Donate" url="/en/donations" %}}
Support our work.
{{% /blocks/feature %}}

{{< /blocks/section >}}

